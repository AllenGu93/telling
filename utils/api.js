/**
 * name: api.js
 */
import request from './request.js'

class api {
    constructor() {
        this.baseUrl = 'https://telling.huhhz.me/api';
        this.appId = 'wx8b021c3828c28bfc';
        this.request = new request;
        this.request.setErrorHandler(this.errorHandler);
    }

    /**
     * 统一的异常处理方法
     */
    errorHandler(res) {
        wx.showToast({
            title: res.data.massage,
            icon: 'none',
            duration: 2000
        })
    }

    // 英雄列表
    login(code, encryptedData, iv) {
        return this.request.postRequest(this.baseUrl + '/auth/login', {
            'code': code,
            'encryptedData': encryptedData,
            'iv': iv,
            'appId':this.appId
        }).then(res => res.data).catch(error => error.data.message)
    }

    me(){
        return this.request.getRequest(this.baseUrl + '/auth/me', {},{
            'Authorization':'Bearer '+this.getUserTokenByStorage()
        }).then(res => res.data).catch(error => error.data.message)
    }

    //获取用户userId
    getUserTokenByStorage() {
        return wx.getStorageSync('userToken')
    }

    //取消loading弹窗
    cancelLoading(time = 1000) {
        setTimeout(function () {
            wx.hideLoading({})
        }, time)
    }
}

export default api